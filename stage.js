'use strict'

const Stage = require('telegraf/stage')
const WizardScene = require('telegraf/scenes/wizard')
const { leave } = Stage

const factory = () => {
  const stage = new Stage()

  stage.command('cancel', leave())

  return stage
}

module.exports = factory
module.exports.WizardScene = WizardScene
