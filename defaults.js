'use strict'

const env = require('sugar-env')

module.exports = {
  telegram: {
    token: env.get('TELEGRAM_TOKEN'),
    username: env.get('TELEGRAM_USERNAME')
  },
  commands: {
    umnatchedText: env.get('COMMANDS_UNMATCHED_TEXT', 'Comando não encontrado')
  },
  server: {
    binding: {
      ip: env.get('SERVER_BINDING_IP', '0.0.0.0'),
      port: parseInt(env.get('SERVER_BINDING_PORT', 3000)),
      address: env.get('SERVER_BINDING_ADDRESS', 'https://localhost')
    }
  }
}
