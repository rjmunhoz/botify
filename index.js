'use strict'

const stage = require('./stage')
const env = require('sugar-env')
const { format } = require('util')
const server = require('./server')
const Telegraf = require('telegraf')
const merge = require('lodash.merge')
const defaults = require('./defaults')
const session = require('telegraf/session')

const CODE_WRAPPER = '```%s```'

const factory = (fn) => {
  process.on('unhandledRejection', (reason, promise) => {
    console.error(reason)
  })

  return (options, environment) => {
    const config = merge(defaults, options)

    const bot = new Telegraf(config.telegram.token, { username: config.telegram.username })

    if (!config.telegram.username) {
      bot.telegram.getMe().then(me => { bot.options.username = me.username })
    }

    bot.use(session())

    bot.use((ctx, next) => {
      if (!ctx.message || !ctx.message.text || !ctx.message.text.startsWith('/')) {
        return next(ctx)
      }

      const start = new Date()

      return next(ctx)
        .then(() => {
          const ms = new Date() - start

          console.log(`${ctx.message.text} - ${ms}ms`)
        })
    })

    bot.command('ping', ({ reply, replyWithMarkdown }) => {
      if (environment !== env.DEVELOPMENT) {
        return reply('pong')
      }

      replyWithMarkdown(format(CODE_WRAPPER, JSON.stringify(config, null, 2)))
    })

    bot.command('id', (ctx) => {
      ctx.replyWithMarkdown(format(CODE_WRAPPER, ctx.from.id), { reply_to_message_id: ctx.message.message_id })
    })

    fn(bot, config)

    bot.hears(/.*/, (ctx) => ctx.reply(config.commands.umnatchedText))

    bot.catch(err => {
      console.error(err)
      process.exit(1)
    })

    return bot
  }
}

module.exports = factory
module.exports.server = server
module.exports.stage = stage
