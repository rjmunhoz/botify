'use strict'

const http = require('http')
const debug = require('debug')
const env = require('sugar-env')
const express = require('express')
const merge = require('lodash.merge')
const defaults = require('./defaults')
/**
 * @param  {Function} botFactory  App factory.
 * @param  {Object}   options     Config object.
 */
const start = (botFactory, options) => {
  const config = merge(
    defaults,
    options
  )

  const app = express()
  const bot = botFactory(config, env.current)

  bot.telegram.setWebhook(`${config.server.binding.address}/${config.telegram.token}`)
  debug('botify:server')(`Telegram webhook set to ${config.server.binding.address}`)

  app.use(bot.webhookCallback(`/${config.telegram.token}`))

  const server = http.createServer(app)

  server.on('listening', () => {
    const addr = server.address()
    debug(`botify:server`)(`Listening on http://${addr.address}:${addr.port}/`)
  })

  server.listen(config.server.binding.port, config.server.binding.ip)
}

module.exports = { start }
